﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineShoppingWebApplication.ConsoleApp.Manage;
using OnlineShoppingWebApplication.Service.Configurations;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.ConsoleApp
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            IConfiguration configuration;
            var services = new ServiceCollection();

            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json")
                .Build();

            services.AddDbContexts(configuration);
            services.AddCoreServices(configuration);
            services.AddSingleton<OnlineShoppingWebApplicationManage>();
            var serviceProvider = services.BuildServiceProvider();
            var applicationManage = serviceProvider.GetService<OnlineShoppingWebApplicationManage>();

            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            await applicationManage.ListManage();
        }
    }
}
