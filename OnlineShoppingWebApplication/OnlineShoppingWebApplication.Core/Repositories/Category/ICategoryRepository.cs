﻿using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<Category> GetByName(string name);
    }
}
