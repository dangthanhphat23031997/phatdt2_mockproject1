﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using System;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(OnlineShoppingWebApplicationDbContext context) : base(context)
        {
        }

        public async Task<Category> GetByName(string name)
        {
            return await _entities.AsNoTracking().FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}
