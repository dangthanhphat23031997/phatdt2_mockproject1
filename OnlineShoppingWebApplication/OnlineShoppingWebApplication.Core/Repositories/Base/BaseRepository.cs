﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories.Base
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly OnlineShoppingWebApplicationDbContext _context;
        protected DbSet<T> _entities;
        public BaseRepository(OnlineShoppingWebApplicationDbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public async Task<IList<T>> GetAll()
        {
            return await _entities.ToListAsync();
        }

        public async Task<T> GetById(Guid id)
        {
            return await _entities.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Add(T entity)
        {
            await _entities.AddAsync(entity);
        }

        public async Task AddRange(IList<T> entity)
        {
            await _entities.AddRangeAsync(entity);
        }

        public void Update(T entity)
        {
            _context.ChangeTracker.Clear();
            _context.Entry(entity).State = EntityState.Detached;
            _entities.Update(entity);
        }

        public async Task DeleteById(Guid id)
        {
            _context.ChangeTracker.Clear();
            T entity = await GetById(id);
            if (entity != null)
                _entities.Remove(entity);
        }

        public void Delete(T entity)
        {
            _context.ChangeTracker.Clear();
            _entities.Remove(entity);
        }
    }
}
