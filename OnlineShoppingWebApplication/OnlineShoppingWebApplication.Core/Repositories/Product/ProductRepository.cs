﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(OnlineShoppingWebApplicationDbContext context) : base(context)
        {

        }

        public async Task<IList<Product>> GetAllIncludeCategory()
        {
            return await _entities.Include(x => x.Category).ToListAsync();
        }

        public async Task<Product> GetByName(string name)
        {
            return await _entities.Include(x => x.Category).AsNoTracking().FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<IList<Product>> GetListByCategoryId(Guid id)
        {
            return await _entities.Include(x => x.Category).Where(x => x.CategoryId == id).ToListAsync();
        }

        public async Task<IList<Product>> GetListByName(string name)
        {
            return await _entities.Include(x => x.Category).Where(x => x.Name.Contains(name)).ToListAsync();
        }
    }
}
