﻿using System.Collections.Generic;

namespace OnlineShoppingWebApplication.Core.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IList<Product> Products { get; set; }
    }
}
