﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace OnlineShoppingWebApplication.Core.Data
{
    public class OnlineShoppingWebApplicationDbContextFactory : IDesignTimeDbContextFactory<OnlineShoppingWebApplicationDbContext>
    {
        public OnlineShoppingWebApplicationDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                        .SetBasePath(Directory.GetCurrentDirectory())
                                        .AddJsonFile("appsettings.json")
                                        .Build();

            var connectionString = configuration.GetConnectionString("OnlineShoppingWebDB");
            var optionsBuilder = new DbContextOptionsBuilder<OnlineShoppingWebApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new OnlineShoppingWebApplicationDbContext(optionsBuilder.Options);
        }
    }
}
