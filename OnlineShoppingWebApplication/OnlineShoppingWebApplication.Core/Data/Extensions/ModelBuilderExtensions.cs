﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Entities;
using System;
using System.Collections.Generic;

namespace OnlineShoppingWebApplication.Core.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder builder)
        {
            IList<Category> categoriesList = new List<Category>();
            Category category = new Category()
            {

                Id = Guid.NewGuid(),
                Name = "Shoes",
                Description = "Shoes"
            };

            Category shirt = new Category()
            {
                Id = Guid.NewGuid(),
                Name = "Shirt",
                Description = "Shirt"
            };

            categoriesList.Add(category);
            categoriesList.Add(shirt);
            builder.Entity<Category>().HasData(categoriesList);

            builder.Entity<Product>().HasData(
                new Product()
                {
                    Id = Guid.NewGuid(),
                    CategoryId = category.Id,
                    Name = "Adidas",
                    Description = "Adidas",
                    Price = 100
                });
        }
    }
}
