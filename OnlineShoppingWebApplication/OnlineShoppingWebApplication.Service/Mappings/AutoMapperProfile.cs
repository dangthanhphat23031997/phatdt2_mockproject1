﻿using AutoMapper;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Service.Services.DTOs;

namespace OnlineShoppingWebApplication.Service.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<CategoryCreateDto, Category>();

            CreateMap<Product, ProductDto>().ReverseMap();
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductImportDto, Product>();
        }
    }
}
