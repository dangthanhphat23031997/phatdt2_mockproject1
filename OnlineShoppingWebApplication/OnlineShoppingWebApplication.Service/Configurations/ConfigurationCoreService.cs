﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineShoppingWebApplication.Core.Repositories;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.Core.Repositories.UnitOfWork;
using OnlineShoppingWebApplication.Service.Services;
using System;

namespace OnlineShoppingWebApplication.Service.Configurations
{
    public static class ConfigurationCoreService
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region Auto Mapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion

            #region DI
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            #endregion

            return services;
        }
    }
}
