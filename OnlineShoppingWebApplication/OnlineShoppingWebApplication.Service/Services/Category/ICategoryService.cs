﻿using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public interface ICategoryService
    {
        Task<ApiResult<bool>> Create(CategoryCreateDto dto);
        Task<ApiResult<bool>> Edit(CategoryDto dto);
        Task<ApiResult<bool>> DeleteByName(string name);
        Task<ApiResult<CategoryDto>> GetByName(string name);
        Task<IList<CategoryDto>> GetAll();
    }
}
