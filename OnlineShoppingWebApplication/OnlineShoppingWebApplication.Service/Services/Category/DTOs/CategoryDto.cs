﻿using System;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
