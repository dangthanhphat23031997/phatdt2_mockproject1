﻿using AutoMapper;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.UnitOfWork;
using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiResult<bool>> Create(CategoryCreateDto dto)
        {
            try
            {
                var categoryOld = await _unitOfWork.CategoryRepository.GetByName(dto.Name);
                if(categoryOld != null)
                {
                    return new ApiErrorResult<bool>($"Category [{dto.Name}] is existing!");
                }
                var category = _mapper.Map<Category>(dto);
                await _unitOfWork.CategoryRepository.Add(category);
                var result = await _unitOfWork.SaveChangesAsync();
                if(result == true)
                {
                    return new ApiSuccessResult<bool>($"Create a category [{dto.Name}] successfully!");
                }
            }
            catch(Exception ex)
            {
                return new ApiErrorResult<bool>($"Error: {ex.Message}");
            }
            return new ApiErrorResult<bool>($"Create a category [{dto.Name}] failed!");
        }

        public async Task<ApiResult<bool>> Edit(CategoryDto dto)
        {
            try
            {
                var categoryOld = await _unitOfWork.CategoryRepository.GetByName(dto.Name);
                if (categoryOld != null && categoryOld.Id != dto.Id)
                {
                    return new ApiErrorResult<bool>($"Category [{dto.Name}] is existing!");
                }
                var category = _mapper.Map<Category>(dto);
                _unitOfWork.CategoryRepository.Update(category);
                var result = await _unitOfWork.SaveChangesAsync();
                if(result == true)
                {
                    return new ApiSuccessResult<bool>($"Edit a category [{dto.Name}] successfully!");
                }
            }
            catch
            {
                return new ApiErrorResult<bool>($"Edit a category [{dto.Name}] failed!");
            }
            return new ApiErrorResult<bool>($"Edit a category [{dto.Name}] failed!");
        }

        public async Task<ApiResult<bool>> DeleteByName(string name)
        {
            try
            {
                var category = await _unitOfWork.CategoryRepository.GetByName(name);
                if (category != null)
                {
                    _unitOfWork.CategoryRepository.Delete(category);
                    var result = await _unitOfWork.SaveChangesAsync();
                    if(result == true)
                    {
                        return new ApiSuccessResult<bool>($"Delete Category [{name}] successfully!");
                    }
                }
                else
                {
                    return new ApiErrorResult<bool>($"Cannot found [{name}]");
                }
            }
            catch(Exception ex)
            {
                return new ApiErrorResult<bool>(ex.Message);
            }
            return new ApiErrorResult<bool>($"Delete Category [{name}] failed!");
        }

        public async Task<ApiResult<CategoryDto>> GetByName(string name)
        {
            try
            {
                var category = await _unitOfWork.CategoryRepository.GetByName(name);
                if(category != null)
                {
                    var result = _mapper.Map<CategoryDto>(category);
                    return new ApiSuccessResult<CategoryDto>(result);
                }
                return new ApiErrorResult<CategoryDto>($"Cannot found [{name}]!");
            }
            catch(Exception ex)
            {
                return new ApiErrorResult<CategoryDto>(ex.Message);
            }
        }

        public async Task<IList<CategoryDto>> GetAll()
        {
            var result = await _unitOfWork.CategoryRepository.GetAll();
            var categoryList = _mapper.Map<IList<CategoryDto>>(result);
            return categoryList;
        }
    }
}
