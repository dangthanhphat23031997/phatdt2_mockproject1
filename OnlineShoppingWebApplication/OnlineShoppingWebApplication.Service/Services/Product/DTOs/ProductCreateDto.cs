﻿using System;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class ProductCreateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public Guid CategoryId { get; set; }
    }
}
