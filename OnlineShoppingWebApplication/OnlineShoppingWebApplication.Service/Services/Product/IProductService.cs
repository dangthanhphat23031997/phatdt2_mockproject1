﻿using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public interface IProductService
    {
        Task<IList<ProductDto>> GetAll();
        Task<IList<ProductDto>> GetAllIncludeCategory();
        Task<ApiResult<bool>> Create(ProductCreateDto dto);
        Task<ApiResult<bool>> Edit(ProductDto dto);
        Task<ApiResult<bool>> DeleteByName(string name);
        Task<ApiResult<ProductDto>> GetByName(string name);
        Task<ApiResult<IList<ProductDto>>> GetListByName(string name);
        Task<IList<ProductDto>> GetListByCategoryId(Guid id);
        Task<ApiResult<bool>> AddRange(IList<ProductImportDto> productImports);
    }
}
