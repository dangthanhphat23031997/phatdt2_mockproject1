﻿namespace OnlineShoppingWebApplication.Service.Common
{
    public struct Constants
    {
        public struct ProductImport
        {
            public struct Header
            {
                public const string No = "No";
                public const string ProductName = "Product Name";
                public const string Price = "Price";
                public const string Category = "Category";
                public const string Description = "Description";
                public const string Log = "Log";
            }
        }
    }
}
